﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Resources;
using WorldOfGates.Properties;
using System.Data.SqlClient;
using System.Configuration;

namespace WorldOfGates
{

    
    class Program
    {    
        static void Main(string[] args)
        {            
            Location.SelectLocations();
            Console.WriteLine("Choose location");
            int ans;
            int.TryParse(Console.ReadLine(), out ans);
            Console.Clear();
            NPC.SelectNPC(ans);
            Console.WriteLine("Choose ally");
            int.TryParse(Console.ReadLine(), out ans);
            Console.Clear();
            Quest.SelectQuestsOfAlly(ans);
            Thread.Sleep(20000);
        }
    }
    class Combat
    {
        
    }
    static  class Location
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["WorldDB"].ConnectionString;    
        
        public static async void SelectLocations()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                SqlCommand command = new SqlCommand("getLocations", conn)
                {
                    // указываем, что команда представляет хранимую процедуру
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                SqlDataReader reader = await command.ExecuteReaderAsync();
                Console.WriteLine($"Locations:");          
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync()) 
                    {
                        Console.Write($"{reader.GetInt32(0)}.");
                        Console.WriteLine($"{reader.GetString(1)}");
                        //Должна производиться передача и возврат объекта DataSet.
                    }
                }
                else
                {
                    Console.WriteLine("Notification Error");
                }
                reader.Close();
            }
        }
        public static async void SelectLocations(string continent)
        {
        }
        public static async void Presentlocation(int id)
        {
            /*NPC.SelectAllies();
            NPC.SelectEnemies();*/
        }
    }
    static class NPC
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["WorldDB"].ConnectionString;
        static public async void SelectEnemies(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
            }
        }
        static public async void SelectAllies(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
            }
        }
        static public async void SelectNPC(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                SqlCommand command = new SqlCommand("getAllies", conn)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                SqlParameter idOfLocParam = new SqlParameter
                {
                    ParameterName = "@idOfLoc",
                    Value = id
                };
                command.Parameters.Add(idOfLocParam);

                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    Console.WriteLine("Allies:");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    while (await reader.ReadAsync())
                    {
                        Console.WriteLine($"{reader.GetInt32(0)}.{reader.GetString(1)}");
                    }
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine("Allies are missing. Try later!");
                }   
                reader.Close();
                command = new SqlCommand("getEnemies", conn)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                idOfLocParam = new SqlParameter
                {
                    ParameterName = "@idOfLoc",
                    Value = id
                };
                command.Parameters.Add(idOfLocParam);

                reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    Console.WriteLine("Enemies:");
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    while (await reader.ReadAsync())
                    {
                        Console.WriteLine($"{reader.GetInt32(0)}.{reader.GetString(1)} - {reader.GetInt32(2)} ");
                    }
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine("Allies are missing. Try later!");
                }
                reader.Close();
            }
        }
    }
    static class Quest
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["WorldDB"].ConnectionString;

        static public async void SelectQuests(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                SqlCommand command = new SqlCommand("getQuest", conn)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@idOfLoc",
                    Value = id
                };
                // добавляем параметр
                command.Parameters.Add(nameParam);
                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {                    
                    while (await reader.ReadAsync())
                    {
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.WriteLine(reader.GetInt32(0));
                        Console.WriteLine(reader.GetString(1));
                        Console.WriteLine(reader.GetString(2));
                    }
                }
                else
                {
                    Console.WriteLine("Quests are missing. Try later!");
                }

                reader.Close();

            }
        }
        static public void SelectQuestsOfAlly(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                SqlCommand command = new SqlCommand("getQuestsOfAlly", conn)
                {
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                SqlParameter nameParam = new SqlParameter
                {
                    ParameterName = "@idOfLoc",
                    Value = id
                };
                // добавляем параметр
                command.Parameters.Add(nameParam);
                SqlDataReader reader = command.ExecuteReader();
                Console.WriteLine("Quests:");
                if (reader.HasRows)
                {
                    string name = reader.GetName(1);
                    object desc = reader.GetName(2);
                    object rew = reader.GetName(3);
                    object exp = reader.GetName(4);
                    object money = reader.GetName(5);

                    while (reader.Read())
                    {
                        Console.WriteLine(name);
                        string questinfo = string.Format("{0}\t{1}", reader.GetInt32(4), reader.GetInt32(5));
                        foreach (char c in reader.GetString(1))
                        {
                            Console.Write(c);
                            Thread.Sleep(150);
                        }
                        Console.Write('\n');
                        Console.WriteLine(desc);
                        foreach (char c in reader.GetString(2))
                        {
                            Console.Write(c);
                            Thread.Sleep(50);
                        }
                        Console.Write('\n');
                        Console.WriteLine(rew);
                        /*foreach (char c in reader.GetInt32(3))
                        {
                            Console.Write(c);
                            Thread.Sleep(100);
                        }*/
                        Console.Write('\n');
                        Console.WriteLine(exp);
                        Console.WriteLine(reader.GetInt32(5));
                        Console.WriteLine(money);
                        Console.WriteLine(reader.GetInt32(6));
                        Console.WriteLine("Will you take this quest?\n y - Yes\n n - No");
                        string a = Console.ReadLine();
                        switch (a)
                        {
                            case "y":
                                Console.Clear();
                                Console.WriteLine("A quest was taken!");
                                break;
                            case "n":
                                Console.Clear();
                                Console.WriteLine("The next quest is...");
                                Thread.Sleep(2000);
                                break;
                            default:
                                Console.Clear();
                                Console.WriteLine("An unknown symbol. Press y or n for 'yes' or 'no'");
                                break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Quests are missing. Try later!");
                }

                reader.Close();

            }
        }
    }     
    static class Auction
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["WorldDB"].ConnectionString;

        static public async void SelectLots()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                SqlCommand command = new SqlCommand("getLots", conn)                
                {
                    // указываем, что команда представляет хранимую процедуру
                    CommandType = System.Data.CommandType.StoredProcedure
                };
                
                SqlDataReader reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    Console.WriteLine("{0}\t{1}\t\t\t{2}\t{3}\t{4}", reader.GetName(0), reader.GetName(1), reader.GetName(2), reader.GetName(3), reader.GetName(4));
                    int id, price, rate, redPrice;
                    string name;

                    while (await reader.ReadAsync())
                    {
                        id = reader.GetInt32(0);
                        name = reader.GetString(1);
                        price = reader.GetInt32(2);
                        rate = reader.GetInt32(3);
                        redPrice = reader.GetInt32(4);

                        if(rate == 4 ) Console.ForegroundColor = ConsoleColor.DarkMagenta;
                        else if(rate == 3) Console.ForegroundColor = ConsoleColor.DarkBlue;
                        else if (rate == 2) Console.ForegroundColor = ConsoleColor.DarkGreen;
                        else if (rate == 1) Console.ForegroundColor = ConsoleColor.White;
                        else if (rate == 0) Console.ForegroundColor = ConsoleColor.Gray;

                        Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", id, name, price, rate, redPrice);
                        Console.ResetColor();
                    }
                }
                else
                {
                    Console.WriteLine("Lots are missing. Try later!");
                }

                reader.Close();
            }
            Console.WriteLine("Press F to buy sth");
        }
        static public async void SearchLots()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
            }
        }
        static public async void InsertLots()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();

            }
        }
        static public async void ChooseLot()
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();

            }
        }
        static public async void BuyLot(int id)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                SqlCommand command = new SqlCommand("DELETE FROM Lots WHERE Id = @id", conn);
                SqlParameter nameParam = new SqlParameter("@id", id);
                // добавляем параметр к команде
                command.Parameters.Add(nameParam);
                await command.ExecuteNonQueryAsync();
            }
        }
    }
    class Auctioneer : GUI
    {
        public  Auctioneer()
        {
            title = lanManager.GetString("auction");
        }
        public override void ShowMenu()
        {
            base.ShowMenu();
            Console.WriteLine("1.Show lots\n2.Sell lots\n3.Previous\n");
            ConsoleKeyInfo c = Console.ReadKey();
            switch (c.Key)
            {
                case ConsoleKey.D1:
                    ShowLots();
                    break;
                case ConsoleKey.D2:
                    break;
                case ConsoleKey.D3:

                    break;
                default:
                    break;
            }

        }       
        public void ShowLots()
        {
            Console.Clear();
            Auction.SelectLots();
            
            ConsoleKeyInfo c1 = Console.ReadKey();
            if (c1.Key == ConsoleKey.F)
            {
                Console.WriteLine("\nChoose lot");
                int choose;
                int.TryParse(Console.ReadLine(), out choose);
                Auction.BuyLot(choose);
                this.ShowMenu();
            }
            else
            {
                this.ShowMenu();
            }
        }
    }
    class GUI
    {
        protected ResourceManager lanManager = new ResourceManager(typeof(Resources));
        protected string title;
        public GUI(string title)
        {
            this.title = lanManager.GetString("title");
        }
        public GUI()
        {            
        }
        virtual public void ShowMenu()
        {
            Console.Clear();
            
        }
    }
    class Item
    {
        string Name { get; set; }
        string Descriptions { get; set; }
        public Item(string name, string des)
        {
            Name = name;
            Descriptions = des;
        }
        
    }
}
